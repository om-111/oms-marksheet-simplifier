# OMS Marksheet Simplifier

## Project Specifications

Name : OMS Marksheet Simplifier  
Written in : Python  
Distributed as : Windows(XP-10) supported exe  
Team Size : Solo  
Current Project Status : Complete  

## Introduction

OMS Marksheet Simplifier is a software executable designed to simplify the process of data
extraction in a suitable format from gradesheets particularly in PDF format which might
contain anywhere from a few to thousands of records.
It is designed to aid Teachers and Institutions to Perform quick data extraction and generate
student data more efficiently. It can efficiently handle multiple records and create CSV Files
pertaining to a set of Pages or to a particular search term.

## Rough Overview of Working

The executable itself can be run directly without any need for installation and without any
need for internet on a target PC. The suitable input format is PDF and the output format is
CSV which makes it versatile in a lot of ways.
The executable accepts a PDF ranging any number of pages and divides it into sections of
100 pages for quicker execution when opened within the executable. You can then directly
click on Extract Data to Extract all records currently present on the selected page range or
you could choose to search for a certain query, like a seat number, college name or student
name and extract that or those specific records.

## Screenshots

### GUI
![GUI](OMS_GUI.png)

### Final Result
![Final Result](FinalOP.png)
